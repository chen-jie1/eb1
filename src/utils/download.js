import axios from "axios";
import cookie from "js-cookie";
const service = axios.create({
  responseType:'arraybuffer'
});
service.interceptors.request.use(config=>{
  config.headers["token"]=cookie.get("token")
  return config
},error=>{
  console.log(error)
})
service.interceptors.response.use(resp=>{
  const headers = resp.headers;
  let reg = RegExp("/application\/json/");
  headers['content-type']//可以获取到响应头中的响应文本类型，可以如果是json就是application/json，如果是流，就是stream

  if(headers['content-type'].match(reg)){ //判断响应内容是不是application/json格式
    resp.data = unitToString(resp.data)
  }else{
    let fileDownload = require('js-file-download')
    console.log(headers['content-disposition']);//获取文件信息，可以拿到文件名和其余信息
    console.log(headers['content-disposition'].split(";")[1]); //吧;后面的内容裁剪出来装到一个数组当中
    let fileName=headers['content-disposition'].split(";")[1].split('filename=')[1]//裁剪出文件名
    console.log(fileName)
    console.log(resp.data); //因为是流，所以返回值是一堆byte数组
    let contentType=headers['content-type']
    fileName=decodeURIComponent(fileName)
    fileDownload(resp.data,fileName,contentType);


  }
},error => {
  console.log(error)
})

function unitToString(unitArray) {
    let encodedString = String.fromCharCode.apply(null,new Uint8Array(unitArray));
    let decodeString = decodeURIComponent(escape(encodedString));
    return JSON.parse(decodeString)
}
let base = ''
export const downloadRequest = (url,params)=>{
  return service({
    method:"get",
    url:`api${base}${url}`,
    data:params
  })
}
export default service;
