import {getRequest} from "./api";


export const initMenu =(router,store)=>{
  if(store.state.routes.length>0){ //如果数据大于0，说明有数据，就直接返回了，不进行初始化
    // console.log("进returb了：此时的路由是：")
    // console.log(store.state.routes)
    return;
  }
   getRequest('/system/cfg/menu').then(data=>{
    if(data){
      let fmtRoutes=formatRoutes(data);
      console.log(fmtRoutes)
      //添加路径到router当中。必须是数组格式
      router.addRoutes(fmtRoutes)
      //将数据都存入到vuex
      store.commit('initRoutes',fmtRoutes)
    }
  })
}
export  const formatRoutes=(data)=>{
  let fmtRoutes=[]
  data.forEach(singleRoute=>{
   // let{path,component,name,iconCls,children}=routes
    let path=singleRoute.path
    let component=singleRoute.component
    let name=singleRoute.name
    let iconCls=singleRoute.iconCls
    let children=singleRoute.children

    if(children && children instanceof Array){
      //这里为什么又要调用formatRoutes(children)？
      //答案：因为children里面的参数有一共9个，看下面列出来的，router里你直接传这9个参数进去一定报错。所以我们需要把这些参数过滤一遍
      //formatRoutes(children)就是在过滤参数，可以理解为格式化参数。留下有用的参数，装入json里面。
      /*
            "id": 19,
             "url": "/statistics/all/**",
             "path": "/sta/all",
             "component": "StaAll",
             "name": "综合信息统计",
             "iconCls": null,
             "keepAlive": null,
             "requireAuth": true,
             "parentId": 5,
             "enabled": null,
             "children": null,
             "roles": null
       */
      children=formatRoutes(children);
    }
    let fmRouter={path:path,name:name,iconCls:iconCls,children:children,
                  component:null
    }
   //   根据条件动态注册组件
    if(component.startsWith("Home")){ //这里Home路径要单拧出来，因为switch语句是截取前三个字符，Home是4个字符。
      fmRouter.component=()=>import('../views/'+component+'.vue')
    }else{
      switch (component.substring(0,3)) {
        case "Emp":{
          fmRouter.component=()=>import('../views/emp/'+component+'.vue')
          break;
        }
        case "Per":{
          fmRouter.component=()=>import('../views/per/'+component+'.vue')
          break;
        }
        case "Sal":{
          fmRouter.component=()=>import('../views/sal/'+component+'.vue')
          break;
        }
        case "Sta":{
          fmRouter.component=()=>import('../views/sta/'+component+'.vue')
          break;
        }
        case "Sys":{
          fmRouter.component=()=>import('../views/sys/'+component+'.vue')
          break;
        }
      }
    }

      // if(component.startsWith("Emp")){
      //   fmRouter.component=()=>import('../views/emp/'+component+'.vue')
      // }else if(component.startsWith("Per")){
      //   fmRouter.component=()=>import('../views/per/'+component+'.vue')
      // }else if(component.startsWith("Sal")){
      //   fmRouter.component=()=>import('../views/sal/'+component+'.vue')
      // }else if(component.startsWith("Sta")){
      //   fmRouter.component=()=>import('../views/sta/'+component+'.vue')
      // }else if(component.startsWith("Sys")){
      //   fmRouter.component=()=>import('../views/sys/'+component+'.vue')
      // }
    fmtRoutes.push(fmRouter)
  })
  return fmtRoutes
}
