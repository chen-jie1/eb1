import axios from 'axios'
import router from '../router/index'
import {Message} from 'element-ui'  //引入elementui里的message消息提示框
import cookie from 'js-cookie'

axios.interceptors.request.use(config=>{
  //如果存在token
  if(cookie.get("token")){
    //将token存入到请求头中
    console.log(cookie.get("token"))
    config.headers["token"]=cookie.get("token")
    return config;
  }else{
   return config;
  }

},error=>{
  console.log(error);
})

axios.interceptors.response.use(success=>{
  //成功返回数据，判断数据中的状态码
  if(success.status && success.status==200){
      if(success.data.code==500 || success.data.code==401 || success.data.code==403){
        Message.error({message:success.data.message});
        if(success.data.message=="令牌已经过期"){
          cookie.remove("token")
          window.location.href=("/")
        }

        return;
      }
      //判断响应包数据里面有没有消息，有的话读出来
      if(success.data.message){
        Message.success({message:success.data.message})
      }
      return success.data
  }
},error=>{ //error：你连接口都访问失败了，后端接口访问不了，一般是服务器宕机了
  console.log(error.response.status)
  console.log(error.response)
  if(error.response.status==500 || error.response.status==404||error.response.status==504){
      Message.error({message:'服务器被吃/(ㄒoㄒ)/~~'})
    }else if (error.response.status == 403){
      Message.error({message:'权限不足请联系管理员'})
    }else if (error.response.status == 401){
      Message.error({message:"尚未登录，请登录"});
      router.replace({path:'/'})
    }else if (error.response.data.message){
      Message.error({message:error.response.data.message})
    }else{
      Message.error({message:'位置错误'})
    }
    return;
})
let base='';
export const postRequest=(url,params)=>{
  return axios({
    method:'post',
    url:`api${base}${url}`,
    data:params
  })
}

export const putRequest=(url,params)=>{
  return axios({
    method:'put',
    url:`api${base}${url}`,
    data:params
  })
}

export const getRequest=(url,params)=>{
  return axios({
    method:'get',
    url:`api${base}${url}`,
    data:params
  })
}

export const deleteRequest=(url,params)=>{
  return axios({
    method:'delete',
    url:`api${base}${url}`,
    data:params
  })
}
