import Vue from 'vue'
import Vuex from 'vuex'
import SockJS from 'sockjs-client'
import Stomp from 'stompjs'
import friendChat from "../views/chat/friendChat";
Vue.use(Vuex)

const now = new Date();

const store =new Vuex.Store({
  state:{
    routes:[],
    userInfo:null,
    currentSessionId:1,
    filterKey:'',
    ws:'',


  },

  mutations:{
    changeCurrentSessionId (state,id) {
      state.currentSessionId = id;
    },
    addMessage (state,msg) {
      state.sessions[state.currentSessionId-1].messages.push({
        content:msg,
        date: new Date(),
        self:true
      })
    },
    INIT_DATA (state) {
      let data = localStorage.getItem('vue-chat-session');
      //console.log(data)
      if (data) {
        state.sessions = JSON.parse(data);
      }
    },

    initRoutes(state,data){
      state.routes=data;
    },
    showUserInfo(state,data){
      state.userInfo=data;
    }
  },
  getters:{

  },
  actions:{
    initData (context) {
      context.commit('INIT_DATA')
    },
    connection(context){
      context.state.stomp
    }
  },
})
export  default store
//next只能执行一次，多次执行会出现 ，next()zhiz
// vue-router - Navigation cancelled from “/” to “/password” with a new navigation
