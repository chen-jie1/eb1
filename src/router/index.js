import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
const originalPush = Router.prototype.push
const originalReplace = Router.prototype.replace
// push
Router.prototype.push = function push (location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}
// replace
Router.prototype.replace = function replace (location, onResolve, onReject) {
  if (onResolve || onReject) return originalReplace.call(this, location, onResolve, onReject)
  return originalReplace.call(this, location).catch(err =>{
    console.log("路由跳转报错，再次跳转#"+location)
    window.location.href="#"+location //跳转页面，因为使用replace和push有可能会报错，
    // 这是router内部的bug导致的，但是报错会导致路由跳转失败，我们 这里需要再跳一次路由。
  })
}
const routes=[
  {
    path:'/',
    name:'login',
    component:()=>import("../views/Login"),
    hidden:true
  },

  {
    path:'/home',
    name:'home',
    component:()=>import("../views/Home"),
    children:[
      {
        path:'/chat',
        name:'在线聊天',
        component:()=>import("../views/chat/friendChat"),
      },
    ]

  }

]
export default new Router({
  routes: routes
})
