
import Vue from 'vue'
import App from './App'
import router from './router'
import elementUi from'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import {postRequest,getRequest,deleteRequest,putRequest} from './utils/api'
import {downloadRequest} from './utils/download'
import store from './store/index'
import {initMenu} from "./utils/menus";
import cookie from 'js-cookie'
import 'font-awesome/css/font-awesome.css'
import fr from "element-ui/src/locale/lang/fr"
import 'element-ui/lib/theme-chalk/index.css';
import Chat from 'jwchat';



Vue.config.productiontip = false
Vue.use(elementUi,{size:'small'})
Vue.use(Chat)
Vue.prototype.postRequest = postRequest;
Vue.prototype.getRequest = getRequest;
Vue.prototype.putRequest = putRequest;
Vue.prototype.deleteRequest = deleteRequest;
Vue.prototype.downloadRequest = downloadRequest;


router.beforeEach(function(to,from,next){

  if(cookie.get("token")){

  // if(!to.path=="/" || to.path=="/login" || to.path=="/logout"){
       initMenu(router,store)
       console.log(2)
       // console.log(to)
       // console.log(from)
       //sessionStorage里别存放token，token太长了，应该会有解析不准确的毛病。
       if(!sessionStorage.getItem("user")){ //取不出来值说明是第一次进来，那么就走下面拿值
         /*
      这里必须return才行，return的目的是为了返回当前对象。sessionStore的坑太多了，稍微不留心就出问题，而且还找不到错误，不建议使用要么vuex要么cookie
      beforeEach方法的调用者肯定是router，和当前js对象不是通一个对象
      所以实际上把值存入到了router对象里面sessionStore当中了。
      这里不写return 那么vue中的sessionStore和这里的sessionStore不是同一个对象。会造成vue里无法到sessionStore里存入的值。
   */
         return getRequest('/admin/info').then(response=>{
           if(response){
             window.sessionStorage.setItem("user",JSON.stringify(response))
             document.title
             document.title=to.name
             next()
           }
         })

    }else{
      document.title=to.name
      next()
    }

  }else{
    if(to.path=='/'){ //用户没有登录，判断用户进入的是不是登录页。
      document.title=to.name
      next();
    }else{
      document.title=to.name
      next("/?redirectPath="+to.path);//用户既没有登录，也没有进登录页。就昂用户跳转到登录页。
      // next是可以传参数的
    }
  }
})
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App,
    // DepMana,
    // PosMana,
    // joblevelMana,
    // EcMana,
    // PermissMana
  },
 // template: '<App/>'  //vue脚手架 ，注释掉这段代码
})
